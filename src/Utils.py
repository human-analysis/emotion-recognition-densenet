import tensorflow as tf 
import numpy as np 
from Variable import Variable 
from Arcface import arcface_logits
# _variable_on_cpu(self, name, shape, initializer, pretrain=True, train=True):
# _variable_with_weight_decay(self, name, stddev_initializer, wd, pretrain=True, train=True):

class DesNet(Variable):
	def __init__(self, common_params, net_params):
		super(DesNet, self).__init__(common_params, net_params)

	def conv2d(self, scope, _input, out_filters, filter_size, strides, use_bias=False, padding=None):
		in_filters = int(_input.get_shape()[-1])
		with tf.variable_scope(scope):
			if padding is not None:
				_input = tf.pad(_input, [[0, 0], [padding, padding], [0, 0]], "CONSTANT")
			filter = self._variable_with_weight_decay(scope + '/Conv_W', [filter_size, filter_size, in_filters, out_filters],
														tf.contrib.layers.variance_scaling_initializer())
			bias = tf.constant(0.0, [out_filter])
			if use_bias == True:
				bias = self._variable_on_cpu(scope + '/Conv_B', [out_filters], tf.constant(0.01, [out_filter]))
			x = tf.nn.conv2d(_input, filter, [1, strides, strides, 1], padding='VALID')
			return tf.nn.bias_add(x, b)

	def avg_pool(self, _input, filter_size, strides):
		return tf.nn.avg_pool(_input, [1, filter_size, filter_size, 1], [1, strides, strides, 1])

	def max_pool(self, _input, filter_size, strides):
		return tf.nn.max_pool(_input, [1, filter_size, filter_size, 1], [1, strides, strides, 1])

	def batch_norm2(self, _input, is_training):
		return tf.contrib.layers.batch_norm(
			_input, scale=True, is_training=is_training, 
			updates_collections=None)
	def batch_norm(self, _input, n_out, phase_train=True, scope='bn'):
		with tf.variable_scope(scope):
			beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
							name='beta', trainable=True)
			gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
							name='gamma', trainable=True)
			batch_mean, batch_var = tf.nn.moments(_input, [0, 1, 2], name='moments')
			ema = tf.train.ExponentialMovingAverage(decay=0.5)

			def mean_var_with_update():
				ema_apply_op = ema.apply([batch_mean, batch_var])
				with tf.control_dependencies([ema_apply_op]):
					return tf.identity(batch_mean), tf.identity(batch_var)

			mean, var = tf.cond(phase_train,
						mean_var_with_update,
						lambda: (ema.average(batch_mean), ema.average(batch_var)))
			normed = tf.nn.batch_normalization(_input, mean, var, beta, gamma, 1e-3)
		return normed
	def dropout(self, _input):
		if self.keep_prob < 1:
			output = tf.cond(
				self.is_training,
				lambda: tf.nn.dropout(_input, self.keep_prob),
				lambda: _input
			)
		else:
			output = _input
		return output

	def composite_function(self, x, out_filters, filter_size=3):
		in_filters = int(x.get_shape()[-1])
		with tf.variable_scope("composite_function"):
			# Batch normalization
			# output = self.batch_norm2(x, self.is_training)
			output = self.batch_norm(x, in_filters, self.is_training)

			# ReLU
			output = tf.nn.relu(output)

			# Conv2d 3x3
			output = self.conv2d(output, out_filters, filter_size, strides=1)
			output = self.dropout(output)

		return output

	def bottleneck(self, x):
		in_filters = int(x.get_shape()[-1])
		out_filters = self.growth_rate * 4
		with tf.variable_scope("bottleneck"):
			# Batch normalization
			# output = self.batch_norm2(x, self.is_training)
			output = self.batch_norm(x, in_filters, self.is_training)

			# ReLU
			output = tf.nn.relu(output)

			# Conv2d 1x1
			output = self.conv2d(output, out_filters, filter_size=1, strides=1, padding='VALID')
			output = self.dropout(output)

		return output

	def add_internal_layer(self, x):
		if self.bc_mode:
			bottleneck = self.bottleneck(x)
			output = self.composite_function(bottleneck, self.growth_rate, filter_size=3)
		else:
			output = self.composite_function(x, self.growth_rate, filter_size=3)

		return tf.concat(axis=3, values=(x, output))

	def add_block(self, input, num_layer):
		output = input
		for layer in range(num_layer):
			with tf.variable_scope("Layer_%d" % layer):
				output = self.add_internal_layer(output)
		return output

	def transition_layer(self, x):
		out_filters = int(int(x.get_shape()[-1]) * self.reduction)
		output = self.composite_function(x, out_filters, filter_size=1)
		output = self.avg_pool(output, 2, 2)
		return output

# def arcface_logits(embedding, labels, out_num, w_init=None, s=64., m=0.5):

	def last_transition_layer(self, x, label, num_class, arcface=False):
		in_filters = int(x.get_shape()[-1])
		# BN
		# output = self.batch_norm2(x, self.is_training)
		output = self.batch_norm(x, in_filters, self.is_training)

		# ReLU
		output = tf.nn.relu(output)

		# Avg pooling
		filter_size = x.get_shape()[-2]
		output = self.avg_pool(output, filter_size, filter_size)

		# Fully connected
		total_features = int(output.get_shape()[-1])
		output = tf.reshape(output, [-1, total_features])
		if not arcface:
			W = tf.get_variable('DW', [total_features, self.num_class], tf.float32,
							initializer=tf.contrib.layers.xavier_initializer())
			b = tf.get_variable('bias', [self.num_class], tf.float32, initializer=tf.constant_initializer(0.0))

			logits = tf.matmul(output, W) + b

			return output, logits, total_features
		else:
			logits, softmax = arcface_logits(output, label, num_class, s=32., m=0.5)

			return output, logits, total_features

	def _inference(self):
		raise NotImplementedError

	def _create_loss(self):
		raise NotImplementedError

	def _create_optimizer(self):
		raise NotImplementedError

