import tensorflow as tf 
 
class Variable():
	def __init__(self, common_params, net_params):
		self.pretrained_collection = []
		self.trainable_collection = []
	def _variable_on_cpu(self, name, shape, initializer, pretrain=True, train=True):
		with tf.device('/cpu:0'):
			var = tf.get_variable(name, shape, initializer=initializer, dtype=tf.float32, trainable=train)
			if pretrain:
				self.pretrained_collection.append(var)
			if train:
				self.trainable_collection.append(var)

		return var

	def _variable_with_weight_decay(self, name, stddev_initializer, wd, pretrain=True, train=True):
		temp = None
		if stddev_initializer.isdigit():
			temp =  tf.truncated_normal_initializer(stddev=stddev_initializer, dtype=tf.float32)
		else:
			temp = stddev_initializer
		var = self._variable_on_cpu(name, shape, temp, pretrain, trainable)
		if wd is not None:
			weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
			tf.add_to_collection('weight_losses', weight_decay)
		return var