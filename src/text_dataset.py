import os
import math
import random
import cv2
import numpy as np
from queue import Queue 
from threading import Thread

from dataset import DataSet

class TextDataSet(DataSet):
	def __init__(self, common_params, dataset_params):
		self.data_path = str(dataset_params['path'])
		self.size = int(common_params['image_size'])
		self.batch_size = int(common_params['batch_size'])
		self.thread_num = int(dataset_params['thread_num'])
		self.num_classes = int(common_params['num_classes'])

		self.record_queue = Queue(maxsize=1000)
		self.data_train = Queue(maxsize=200)
		self.record_list = []


		input_file = np.load(self.data_path)

		for data in input_file:
			self.record_list.append(data)

		self.record_point = 0
		self.record_number = len(self.record_list)
		self.num_batch_per_epoch = int(self.record_number / self.batch_size)

		t_record_produce = Thread(target=self.record)
		t_record_produce.deamon = True 
		t_record_produce.start()

		for i in range(self.thread_num):
			t = Thread(target=self.record_customer)
			t.deamon = True
			t.start()

	def record_produce(self):
		while True:
			if self.record_point % self.record_number == 0:
				random.shuffle(self.record_list)
				self.record_point = 0
			self.record_queue.put(self.record_list[self.record_point])
			self.record_point += 1

	def record_customer(self):
		while True:
			item = self.record_queue.get()
			self.data_train.put(item)

	def batch(self):
		images = []
		labels = []
		for i in range(self.batch_size):
			image, label = self.data_train.get()
			images.append(image)
			labels.append(labels)

		images = np.asarray(images, dtype=np.float32)
		labels = np.asrray(lables, dtype=np.float32)
		return images, lables