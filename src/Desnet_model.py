from Utils import DesNet
import tensorflow as tf 
import numpy as np 
import os

class DesNet_CNN(DesNet):
	def __init__(self, common_params, net_params):
		super(DesNet_CNN, self).__init__(common_params, net_params)
		'''all i need is here'''
		self.num_class = int(common_params['num_class'])
		''''''
		
	def _inference(self, _input, y_, label, arcface=False):
		self.layer_per_block = int((self.depth - self.total_blocks - 1) / self.total_blocks)
		'''with first conv filter 3x3x(2*growth_rate)'''
		with tf.variable_scope('Init_conv') as scope:
			output = self.conv2d(_input, scope, out_filters=2*growth_rate, filter_size=3, strides=1)

		'''Desnet_block'''
		for block in range(self.total_blocks):
			with tf.variable_scope("Block_%d" %block):
				output = self.add_block(output, self.layer_per_block)

				print("Shape after desnet block ", str(block), " : ", output.get_shape())

				if block != self.total_blocks - 1:
					with tf.variable_scope("Transition_after_block_%d"%block):
						# def last_transition_layer(self, x, label, num_class, arcface=False):
						output = self.trainsition_layer(output, label, self.num_class, arcface)

					print("Shape after transition ", str(block), " : ", output.get_shape())
		'''Last Transition for classification class'''
		
		with tf.variable_scope("Transition_to_classes"):
			self.score, self.logits, self.num_feature = self.last_transition_layer(output)
		print("Shape after last block: ", self.logits.get_shape())

		prediction = tf.nn.softmax(self.logits)
		correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(y_, 1))
		accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
		return prediction, correct_pred, accuracy

	def update_center(self, label):
		unique_label, unique_idx, unique_count = tf.unique_with_counts(label)
		self.appear_times = tf.gather(unique_count, unique_idx) + 1
		self.appear_times = tf.cast(self.appear_times, tf.float32)
		self.appear_times = tf.reshape(self.appear_times, [-1, 1])

		self.update = tf.div(self.diff, self.appear_times)
		update_centers_op = tf.scatter_sub(self.centers, label, 0.5 * self.update)
		return update_centers_op

	def marginal_loss(self, label, thre = 1.2, slack = 0.3):
		# norm
		A = self.score
		A_l2 = tf.sqrt(tf.reduce_sum(A ** 2, 1, keep_dims=True))
		A_norm = A / A_l2
		# Distance
		r = tf.reduce_sum(A_norm * A_norm, 1)
		# turn r into column vector
		r = tf.reshape(r, [-1, 1])
		distance = r - 2 * tf.matmul(A_norm, tf.transpose(A_norm)) + tf.transpose(r)

		# yij
		y = label
		y = tf.one_hot(y, self.num_class)# chuyen thanh one- hot
		r = tf.reduce_sum(y * y, 1)
		r = tf.reshape(r, [-1, 1])
		C = r - 2 * tf.matmul(y, tf.transpose(y)) + tf.transpose(r)

		same = 1-C
		matrix_sub = (slack - same * (thre - distance))
		return tf.reduce_sum(tf.nn.relu( matrix_sub))

	def _create_loss(self, label, logits, y_, weight_decay):
		self.centers = self._variable_on_cpu('centers', [self.num_class, self.num_feature],
													tf.constant_initializer(0.0), train=False)
		self.centers_batch = tf.gather(self.centers, label)
		self.diff = self.centers_batch - self.score
		self.center_loss = 1.0 * tf.nn.l2_loss(self.diff)
		self.margin_loss = 1e-4 * self.marginal_loss(label, self.num_class)
		# self.l2_loss = tf.add_n([tf.nn.l2_loss(var) for var in self.trainable_collection])
		l2_loss = tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])
		cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, lables=y_))
		total_loss = weight_decay * l2_loss + cross_entropy
		return total_loss, l2_loss, cross_entropy
	