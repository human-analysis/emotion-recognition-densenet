import tensorflow as tf 
import numpy as np 
import re
import sys 
import time
import os
import Desnet_input

from solver import Solver 

class Desnet_solver(Solver):
	def __init__(self, dataset, net, common_params, solver_params, arcface=False, test_during_train=True, test=False, snapshot_test=False):
		self.moment = float(solver_params['moment'])
		self.init_lnr = float(solver_params['learning_rate'])
		self.batch_size = int(common_params['batch_size'])
		self.input_size = int(common_params['image_size'])
		self.num_class = int(common_params['num_class'])
		self.pretrain_path = str(solver_params['pretrain_model_path'])
		self.train_dir = str(solver_params['train_dir'])
		self.valid_dir = str(solver_params['valid_dir'])
		self.path_data_test = str(solver_params['test_dir'])

		self.max_iterators = int(solver_params['max_iterators'])
		self.reduce_lnr = int(common_params['reduce_lnr'])
		self.arcface = arcface
		self.dataset = dataset
		self.net = net
		#construct graph
		# self.snapshot_test = snapshot_test
		if not snapshot_test:
			self.construct_graph()
		# self._input()
		# self.inference()
		# self.losses()
		# self.train_step()
		# self.count_trainable_params()
		# self.init_session()
		self.test_during_train = test_during_train
		if test:
			self.test_data = np.load(self.path_data_test+'/public_test.npy')


	def _train(self):
		self.learning_rate = tf.placeholder(tf.float32)
		self.train_step = tf.train.MometumOptimizer(learning_rate=0.01, momentum=0.9,
													use_nesterov=True).minimize(self.total_loss,
																				global_step=self.global_step)
		self.train_step2 = tf.train.MometumOptimizer(learning_rate=self.default_lr, momentum=0.9,
													use_nesterov=True).minimize(self.center_loss)

		self.update_centers_op = self.net.update_centers()
		self.train_step3 = tf.train.MomentumOptimizer(learning_rate=1e-3, momentum=0.9, use_nesterov=True).minimize(
													self.margin_loss)

	def construct_graph(self, snapshot_test=False):
		# construct graph
		self._input = tf.placeholder(tf.float32, [None, self.input_size, self.input_size, self.num_chanel])
		self.y_ = tf.placeholder(tf.float32, [None, self.num_class])
		self.label = tf.argmax(self.y_, axis=1)
		self.is_training = tf.placeholder(tf.bool)
		# def _inference(self, _input, y_, label, num_class, arcface=False):
		# def _create_loss(self, label, logits, y_, num_class, score, weight_decay):
		self.prediction, self.correct_pred, self.accuracy = self.net._inference(self._input, self.y_, self.label, self.num_class, self.arcface)
		# def _create_loss(self, label, logits, y_, weight_decay):
		self.total_loss, self.l2_loss, self.cross_entropy = self.net._create_loss(self.label, self.net.logits, self.y_, self.num_class, self.weight_decay)#issue
		if not snapshot_test:
			self._train()
		self.count_trainable_params()
		# self.init_session()

	def count_trainable_params(self):
		total_parameters = 0
		for variable in tf.trainable_variables():
			shape = variable.get_shape()
			variable_parameters = 1
			for dim in shape:
				variable_parameters *= dim.value
			total_parameters += variable_parameters
		print("Total training params: %.1fM" % (total_parameters / 1e6))

	def init_session(self):
		if not os.path.isdir(self.train_dir):
			os.mkdir(self.train_dir)
			os.mkdir(os.path.join(self.train_dir, 'current'))

		if not os.path.isdir(self.logs_folder):
			os.mkdir(self.logs_folder)
		self.train_dir = os.path.join(self.train_dir, 'current') + '/'
		self.sess = tf.InteractiveSession()
		self.saver = tf.train.Saver(max_to_keep=self.max_to_keep)
		if not os.path.isfile(self.train_dir + 'model.ckpt.index'):
			print('Create new Model')
			self.sess.run(tf.global_variables_initializer())
			print('OK')
		else:
			print('Restoring existed model')
			self.saver.resotore(self.sess, self.train_dir + 'model.ckpt')
			print('OK')
			print(self.global_step.eval())
		writer = tf.summary.FileWriter
		self.summary_writer = writer(self.logs_folder)

	def solve(self, max_epoch):
		self.init_session()
		num_batch = int(self.dataset.num_batch)
		cuurent_epoch = int(self.global_step.eval() / num_batch)
		lnr = self.init_lnr

		for epoch in range(cuurent_epoch + 1, max_epoch):
			cuurent_epoch = int(self.global_step.eval())
			print('Epoch:', str(epoch))
			start_time = time.time()
			for i in range(len(self.reduce_lnr)):
				if self.reduce_lnr[i] <= epoch:
					lnr = self.init_lnr * (0.1 ** (i + 1))

			print("learning_rate: %f" % lnr)
			sum_loss = []
			sum_l2 = []
			sum_acc = []
			sum_centre_loss = []
			sum_margin_loss = []
			for batch in range(num_batch):
				batch_img, batch_label = self.dataset.batch_size
				batch_img = Desnet_input.augmentation(batch_img, self.input_size)

				ttl, l2l, ctl, mgl, _, __, ___, ____, acc = self.sess.run(
					[self.total_loss, self.l2_loss, self.center_loss, self.net.margin_loss, self.train_step, self.train_step2, self.train_step3
					self.update_centers_op, self.accuracy],
								feed_dict={self._input: batch_img, self.y_: batch_label,
											self.is_training: True, self.learning_rate: lnr})

				print('Training on batch %s / %s' % (str(batch + 1), str(num_batch)), end='\r')
				sum_loss.append(ttl)
				sum_acc.append(acc)
				sum_l2.append(l2l)
				sum_centre_loss.append(ctl)
				sum_margin_loss.append(mgl)
			time_per_epoch = time.time() - start_time
			mean_loss = np.mean(sum_loss)
			mean_acc = np.mean(sum_acc)
			mean_l2 = np.mean(sum_l2)
			mean_centre_loss = np.mean(sum_centre_loss)
			mean_margin_loss = np.mean(sum_margin_loss)
			print('\nTraining time: ', str(timedelta(seconds=time_per_epoch)))
			print('Training loss: %f' % mean_loss)
			print('L2 loss: %f' % mean_l2)
			print('Centre loss: %f' % mean_centre_loss)
			print('Margin loss: %f' % mean_margin_loss)
			print('Train accuracy: %f' % mean_acc)
			self.saver.save(self.sess, save_path=self.train_dir + 'model' + str(epoch) + '.ckpt')
			self.saver.save(self.sess, save_path=self.train_dir + 'model.ckpt')
			self.log_loss_accuracy(loss=mean_loss, accuracy=mean_acc, epoch=epoch, prefix='train')

			mean_loss, mean_acc = self.valid(batch_size=batch_size)
			self.log_loss_accuracy(loss=mean_loss, accuracy=mean_acc, epoch=epoch, prefix='valid')
			if mean_acc >= 0.67:
				self.saver.save(self.sess, save_path=self.valid_dir + 'model' + str(round(mean_acc, 4)) + '.ckpt')

			if self.test_during_train:
				mean_loss, mean_acc, _, _ = self.test(batch_size=batch_size)
				self.log_loss_accuracy(loss=mean_loss, accuracy=mean_acc, epoch=epoch, prefix='test')

	def log_loss_accuracy(self, loss, accuracy, epoch, prefix):
		summary = tf.Summary(value=[
			tf.Summary.Value(
				tag='loss_%s' % prefix, simple_value=float(loss)),
			tf.Summary.Value(
				tag='accuracy_%s' % prefix, simple_value=float(accuracy))
		])
		self.summary_writer.add_summary(summary, epoch)


	def test(self, batch_size=1, save_file=None):
		print('\n')
		if save_file is not None:
			self.sess.close()
			self.sess = tf.InteractiveSession()
			print("Restoring session from %s for testing" % save_file)
			self.saver.restore(self.sess, self.train_dir + save_file)
			print("OK")
		all_score = []
		all_label = []
		test_img = []
		test_label = []
		for i in range(len(self.test_data)):
			test_img.append(self.test_data[i][0])
			test_label.append(self.test_data[i][1])

		num_batch = int(len(test_img) // batch_size)
		sum_loss = []
		sum_acc = []
		for batch in range(num_batch):
			top = batch * batch_size
			bot = min((batch + 1) * batch_size, len(self.valid_data))
			batch_img = np.asarray(test_img[top:bot])
			batch_label = np.asarray(test_label[top:bot])

			batch_img = Desnet_input.augmentation(batch_img, self.input_size, testing=True)
			logits = self.sess.run([self.prediction],
						feed_dict={self._input: batch_img, self.y_: batch_label, self.is_training: False})
			logits = np.asarray(logits)
			for i in range(logits.shape[1]):
				all_score.append(logits[0][i])
				all_label.append(batch_label[i])

			ttl, l2l, acc = self.sess.run([self.total_loss, self.l2_loss, self.accuracy],
											feed_dict={self._input: batch_img, self.y_: batch_label,
											self.is_training: False})
			print('Testing on batch %s / %s' % (str(batch + 1), str(num_batch)), end='\r')
			sum_loss.append(ttl)
			sum_acc.append(acc)
		mean_loss = np.mean(sum_loss)
		mean_acc = np.mean(sum_acc)
		print('\nTest loss: %f' % mean_loss)
		print('Test accuracy: %f' % mean_acc)
		return mean_loss, mean_acc, all_score, all_label

	def test_snapshot_ensemble(self, batch_size=1, save_file=None):
		test_img = []
		test_label = []
		for i in range(len(self.test_data)):
			test_img.append(self.test_data[i][0])
			test_label.append(self.test_data[i][1])

		num_batch = int(len(test_img) // batch_size)
		prediction = np.zeros([len(test_img), self.num_class])
		num_saves = len(save_file)
		all_label = []
		for i in range(num_saves):
			print("Checkpoint %d" % save_file[i])
			tf.reset_default_graph()
			sess = tf.InteractiveSession()
			self.construct_graph(snapshot_test=True)

			saver = tf.train.Saver()
			saver.restore(sess, self.valid_dir + "model{}.ckpt".format(save_file[i]))
			for batch in range(num_batch):
				top = batch * batch_size
				bot = min((batch + 1) * batch_size, len(self.test_data))
				batch_img = np.asarray(test_img[top:bot])
				batch_label = np.asarray(test_label[top:bot])

				if i == 0:
					all_label.append(batch_label[i])

				batch_img = DenseNet_input.augmentation(batch_img, self.input_size, testing=True)
				logits = sess.run([self.logits],
								feed_dict={self.x: batch_img, self.y_: batch_label, self.is_training: False})
				logits = np.asarray(logits)
				prediction[top] += logits[0][0]

		correct_pred = np.equal(np.argmax(prediction, 1), np.argmax(np.asarray(test_label), 1))
		accuracy = np.mean(correct_pred.astype(int))
		# print("Snapshot test accuracy: {}".format(accuracy))
		# return prediction, all_label
