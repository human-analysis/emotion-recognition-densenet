import sys
from Desnet import DesNet_CNN
import tensorflow as tf 
import numpy as np 
import cv2 


classes_name = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']

common_params = {'input_size': 48, 'num_class': 20, 
				'batch_size':1}
net_params = {'cell_size': 7, 'boxes_per_cell':2, 'weight_decay': 0.0005}

net = DesNet_CNN(common_params, net_params)

self._input = tf.placeholder(tf.float32, [1, 48, 48, 3])
self.y_ = tf.placeholder(tf.float32, [None, self.num_class])
self.label = tf.argmax(self.y_, axis=1)