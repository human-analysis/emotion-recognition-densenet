import tensorflow as tf 
import numpy as np 
import pickle
import math
import random
from threading import Thread 
from concurrent.futures import ThreadPoolExecutor, wait
from Queue import Queue

class Dataset():
	def __init__(self, common_params, dataset_params):
		self.data_path = str(dataset_params['path'])
		self.size = int(common_params['image_size'])
		self.batch_size = int(common_params['batch_size'])
		self.thread_num = int(dataset_params['thread_num'])
		self.num_classes = int(common_params['num_classes'])

		self.record_queue = Queue(maxsize=1000)
		self.data_train = Queue(maxsize=200)
		self.record_list = []


		input_file = np.load(self.data_path)

		for data in input_file:
			self.record_list.append(data)

		self.record_point = 0
		self.record_number = len(self.record_list)
		self.num_batch_per_epoch = int(self.record_number / self.batch_size)

		t_record_produce = Thread(target=self.record)
		t_record_produce.deamon = True 
		t_record_produce.start()

		for i in range(self.thread_num):
			t = Thread(target=self.record_customer)
			t.deamon = True
			t.start()

	def record_produce(self):
		while True:
			if self.record_point % self.record_number == 0:
				random.shuffle(self.record_list)
				self.record_point = 0
			self.record_queue.put(self.record_list[self.record_point])
			self.record_point += 1

	def record_customer(self):
		while True:
			item = self.record_queue.get()
			self.data_train.put(item)

	def batch(self):
		images = []
		labels = []
		for i in range(self.batch_size):
			image, label = self.data_train.get()
			images.append(image)
			labels.append(labels)

		images = np.asarray(images, dtype=np.float32)
		labels = np.asrray(lables, dtype=np.float32)
		return images, lables

class Dataset_tf():
	def __inti__(self, common_params, dataset_params):
		self.data_path = str(dataset_params['path'])
		self.size = int(common_params['image_size'])
		self.batch_size = int(common_params['batch_size'])
		self.thread_num = int(dataset_params['thread_num'])
		self.num_classes = int(common_params['num_classes'])
		self.train_number = int(dataset_params['train_number'])

		self.train_point = 0
		self.test_point = 0

		self._make_input()

	def self._make_input(self):
		self.is_training = tf.placeholder(tf.int32, [])
		self._input = tf.placeholder(shape=[self.size, self.size, 1], dtype = tf.float32)

		self.train_queue = tf.FIFOQueue(capacity = self.batch_size*10,
										dtypes = [tf.float32],
										shape = [self.size, self.size, 1], 
										shared_name = "train_queue")
		self.test_queue = tf.FIFOQueue(capacity = self.batch_size*9,
										dtypes = [tf.float32, tf.float32],
										shape = [[self.size, self.size, 1], [self.size, self.size, 1]], 
										shared_name = "test_queue")
		self.queue = tf.QueueBase.from_list(self.is_training, [self.train_queue, self.test_queue])

		self.train_queue_op = self.train_queue.enqueue([self._input])
		self.test_queue_op = self.test_queue.enqueue([self._input])

		self._train_queue_close = self.train_queue.close(cancel_pending_enqueues=True)
		self._test_queue_close = self.test_queue.close(cancel_pending_enqueues=True)

	def batch(self):
		iamges, labels = self.queue.dequeue_up_to(self.batch_size)

	def close_queue(self, session):
		session.run(self._queue.close)

	def _pre_batch_queue(self, sess, coord, is_training):
		while not coord.should_stop():
			if is_training:
				if self.train_point % self.train_number == 0:
					random.shuffle()