import numpy as np 
import pandas as pd 
import pickle
import os 

data_path = './original_data/fer2013.csv'
pre_data = './processed_data/'
data = pd.read_csv(data_path)
print(data.head())

train_data = []
private_data = []
public_data = []

num_samples = np.zeros([7])

def one_hot_vector(label):
	res = np.zeros(7)
	res[label] = 1.0
	return res 

def data_to_image(pixels):
	data_image = np.fromstring(pixels, dtype=np.uint8, sep=' ')
	return data_image

for index, row in data.iterrows():
	pix = np.reshape(data_to_image(row['pixels']), (48, 48, 1)) / 255.0
	num_samples[int(row['emotion'])] += 1
	emotion = one_hot_vector(int(row['emotion']))
	usage = row['Usage']
	if row['Usage'] == 'Training':
		train_data.append((pix, emotion))
	else:
		if row['Usage'] == 'PublicTest':
			public_data.append((pix, emotion))
		else:
			private_data.append((pix, emotion))

print(num_samples)
if not os.path.isdir(pre_data):
	os.mkdir(pre_data)
np.save(pre_data+'/train.npy', train_data)
np.save(pre_data+'/public_test.npy', public_data)
np.save(pre_data+'/private_data.npy', private_data)

